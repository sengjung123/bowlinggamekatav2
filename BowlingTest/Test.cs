﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingTest
{
    [TestFixture()]
    public class Test
    {
        //[SetUp]
        //Game game = new Game;

        //public void rollMany(int rolls, int pins){
        //    for (int i = 0, rolls <20, i++)
        //}

        [Test]
        public void TestCase()
        {
            Game game = new Game();
        }
        [Test]
        public void GutterGame()
        {
            Game game = new Game();
            int i = 0;
            while (i < 20)
            {
                game.roll(0);
                i++;
            }
            Assert.That(game.score(), Is.EqualTo(0));
        }
        [Test]
        public void OnePinFall()
        {
            Game game = new Game();
            int i = 0;
            while (i < 20)
            {
                game.roll(1);
                i++;
            }
            Assert.That(game.score(), Is.EqualTo(20));
        }

        [Test]
        public void OneSpare()
        {
            Game game = new Game();
            int i = 0;
            game.roll(5);
            game.roll(5);
            while (i<18)
            {
                game.roll(1);
                i++;
            }
            Assert.That(game.score(), Is.EqualTo(29));
        }


    }
}
